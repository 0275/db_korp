﻿/*База данных их трех сотрудников. У каждого сотрудника установлен свой оклад, премии по
  умолчании нет. В МЕНЮ_1 выводится список сотрудников и возможность выбрать нужного,
  либо выйти из программы. После выбора сотрудника консоль очищается и появляется МЕНЮ_2.
  В нем можно изменить оклад или дабавить премию +10% к окладу, после чего можно выбрать
  пункт меню и пересчить харплату. Из МЕНЮ_2 можно вернуться в МЕНЮ_1 или выйти из программы*/
#include <iostream>
using namespace std;
int menu(), menu2();
int main()
{
    setlocale(LC_ALL, "Rus");
    int choice, choice2, choice3;
    const int REW = 4;
    char   name[REW][80]{ "","1. Александр  Шрам", "2. Максим Владыкин","3. Алексей Тимушев" };
    float  wage[REW]{ 0,75000, 99999, 50000 }; // базовые оклады
    float prize[REW]{ 0,0,0,0 };// базовые премии
    cout << "СПИСОК СОТРУДНИКОВ КОРПОРАЦИИ" << endl << endl;

    for (int i = 1; i < REW; i++) // заполняю массив именами.
    {
        cout << name[i] << endl;
    }
    cout << endl;

    do
    {
        choice2 = menu();
        switch (choice2)
        {
        case 1:
            cout << "Ведите номер сотрудника: ";
            cin >> choice; // эта переменная для выбора сотрудника, она же для индекса массива зп и оклада
            cout << endl << "ты выбрал сотрудника" << endl
                << name[choice] << " его оклад= "
                << wage[choice] << " руб., " << " его премия= "
                << prize[choice] << " руб. " << endl;
            break;
        }
        break;
   } while (choice2 != 0);  ////////////////////////////////////подменю после выбора сотрудника
    do //if (choice3 !=0)
    {
        choice3 = menu2();
        switch (choice3)
        {
        case 1:
            cout << "введите новый оклад= ";
            cin >> wage[choice];
            cout << "Сотруднику " << name[choice] << " установлен оклад "
                << wage[choice] << " руб.";
            break;
        }
    } while (choice3 != 0);

    //    /*case 2:
    //        cout << "введите новый оклад= ";
    //        cin >> wage[choice];
    //        cout << "Сотруднику " << name[choice] << " установлен оклад "
    //            << wage[choice] << " руб.";
    //        break;
    //    }*/
    //    /*do
    //    {
    //        choice2 = menu2();
    //        switch (choice2)
    //        {
    //        case 1:
    //            cout << "введите новый оклад= ";
    //            cin >> wage[choice];
    //            cout <<"Сотруднику "<< name[choice] <<" установлен оклад "<< wage[choice]<< " руб.";
    //        }
    //        break;
    //    } while (choice2 != 0);*/

}
int menu() // меню выбора сотрудника. 
{
    int choice2;
    cout << endl;
    cout << "Для выбора сотрудника нажмите 1? \n";
    cout << "Для выхода нажмите 0? \n";
    cin >> choice2;
    return choice2;
}

int menu2() // меню выбора денег
{
    int choice3;
    cout << endl;
    cout << "1. Ввести новый оклад? \n";
    cout << "2. Дать премию? \n";
    cout << "3. Посчитать зарплату? \n";
    cout << "0. Выход? \n";
    cin >> choice3;
    return choice3;
}

// сохранил данную версию перед сном